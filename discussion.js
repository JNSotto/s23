// CRUD - Create, Read, Update and Delete

//Create
	// - allows us to create documents under a collection or create a collection if it does not exist yet. 
	
	//Syntax: db.collection.insertOne()
		//-allows to insert or create one document.
		//collections - stands for collection name
	
	db.users.insertOne(
		{
			"firstName":"Tony",
			"lastName": "Stark",
			"userName": "iAmIronMan",
			"email":"iloveyou3000@mail.com",
			"password":"starkIndustries",
			"isAdmin":true
		}
	)

	//Syntax: db.collection.insertMany()
		//- allows to insert or create one document.
	
	db.users.insertMany([
	{
			"firstName":"Pepper",
			"lastName": "Potts",
			"userName": "rescueArmor",
			"email":"pepper@mail.com",
			"password":"whereIsTonyAgain",
			"isAdmin":false
	},
	{
			"firstName":"Steve",
			"lastName": "Rogers",
			"userName": "theCaptain",
			"email":"captAmerica@mail.com",
			"password":"iCanLiftMjorlnirToo",
			"isAdmin":false
	},
	{
			"firstName":"Thor",
			"lastName": "Odinson",
			"userName": "mightyThor",
			"email":"ThorNotLoki@mail.com",
			"password":"iAmWorthyToo",
			"isAdmin":false
	},
	{
			"firstName":"loki",
			"lastName": "Odinson",
			"userName": "godOfMischief",
			"email":"Loki@mail.com",
			"password":"iAmRealLoki",
			"isAdmin":false
	},
		])

/*
	mini-activity
		1. Make a new collection with the name courses
		2. Insert the following fields and values:
			name: JavaScript,
			price:3500,
			description: Learn JavaScript in a week!,
			isActive: true

			name: HTML,
			price:1000,
			description: Learn basic HTLM in 3 days!,
			isActive: true

			name: CSS,
			price:2000,
			description: Make your Website fancy, learn CSS now!,
			isActive: true

 */

	db.courses.insertMany([
	{
			"name": "JavaScript",
			"price":3500,
			"description": "Learn JavaScript in a week!",
			"isActive": true
	},
	{
			"name": HTML,
			"price":1000,
			"description":" Learn basic HTLM in 3 days"!,
			"isActive": true

	},
	{
			"name": CSS,
			"price":2000,
			"description": "Make your Website fancy, learn CSS now!",
			"isActive": true

	},
	])



//READ
	// allows us to retrieve data
	// it needs a query or filters to specify the document we are retrieving. 
	// Sxntax: db.collection.find()
		//allows us to retrieve ALL documents in the collection.
db.users.find();


	//Syntax: db.collection.findOne({criteria:value})
		//allows us to find the document that matches our criteria
db.users.find({"isActive":false});


	//Syntax: db.collection.findOne({})
		//allows to find the first document. 
db.users.findOne({});

	
	//Syntax: db.collection.find({,})
		//allows us to find the document that satisfy all criterias.
db.users.find({"lastName":"Odinson","firstName":"loki"})



//UPDATED
	// allows to update documents. 
	// also use criteria or filter. 
	// $set operator
	// REMINDER: Updates are permanent and can't be rolled back.
	
	//Syntax: db.collection.updateOnedb.users.updateOne({"lastName":"Potts"},{$set:{"lastName":"Stark"}})
		//allows us to update one document that first satisfy the criteria
db.users.updateOne({"lastName":"Potts"},{$set:{"lastName":"Stark"}})

	//Syntax:db.collection.updateMany({"lastName":"Potts"},{$set:{"lastName":"Stark"}})
		//allows us to update ALL document that satisfy the criteria
db.users.updateMany({"lastName":"Odinson"},{$set:{"isAdmin":true}})

	//Syntax: db.collection.updateOne({},{$set:{"lastName":"Stark"}})
		//allows us to update the first item in the collection. 
db.users.updateOne({},{$set:{"email":"starkIndustries@mail.com"}})
	// it also allows to add objects to the collection
db.courses.updateMany({},{$set:{"enrollees":10}})

/*
	Mini- Activity
		1. update the Javascript course
			- make isActive: false
 */

db.courses.updateOne({"name":"JavaScript"},{$set:{"isActive":false}})

db.courses.updateMany({},{$set:{"enrollees":10}})

//DELETE
	//allows us to delete documents. 
	//provide criteria or filters to specify which document to delete from the collection. 
	//REMINDER: Be careful when deleting documents, because it will be complicated to retrieve them back again.
	
	//Syntax: db.collection.deleteOne({criteria:value})
		//allows us to delete teh first item that matches our criteria

db.users.deleteOne({"isAdmin":false})
	
	//Syntax: db.collection.deleteMany({criteria:value})
		//allows us to delete all items that matched our criteria
db.users.deleteMany({"lastName":"Odinson"})

	//Syntax: db.collection.deleteOne({})
		//allows us to delete the first document. 
db.users.deleteOne({})

	//Syntax: db.collection.deleteMany({})
		//allows us to delete ALL in the collections.
db.users.deleteMany({})